package check

import (
	"database/sql"
	"fmt"
	"strings"
)

type ClickHouse interface {
	DB() *sql.DB
	Version() (*Version, error)
	ShowDatabases() ([]string, error)
	ShowTables(from ...string) ([]string, error)
	DatabaseExists(database string) (bool, error)
	TableExists(database, table string) (bool, error)
	DictionaryExists(dictionary string) (bool, error)
	ReloadDictionary(dictionary string) error
}

type Version struct {
	Major int
	Minor int
	Patch int
}

func (v *Version) Less(v2 *Version) bool {
	var (
		majorEq = v.Major == v2.Major
		minorEq = majorEq && v.Minor == v2.Minor
	)
	return v.Major < v2.Major || (majorEq && v.Minor < v2.Minor) || (minorEq && v.Patch < v2.Patch)
}

func (v *Version) String() string {
	return fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.Patch)
}

func Connect(dsn string) (ClickHouse, error) {
	conn, err := sql.Open("clickhouse", dsn)
	if err != nil {
		return nil, err
	}
	return &client{
		conn: conn,
	}, nil
}

type client struct {
	conn *sql.DB
}

func (c *client) DB() *sql.DB {
	return c.conn
}

func (c *client) Version() (*Version, error) {
	var version Version
	const query = `
		WITH (splitByChar('.',version())) AS version
		SELECT
			toInt16(version[1])   AS major
			, toInt16(version[2]) AS minor
			, toInt16(version[3]) AS patch
	`
	if err := c.conn.QueryRow(query).Scan(&version.Major, &version.Minor, &version.Patch); err != nil {
		return nil, err
	}
	return &version, nil
}

func (c *client) ShowDatabases() (databases []string, _ error) {
	rows, err := c.conn.Query("SHOW DATABASES")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var database string
		if err := rows.Scan(&database); err != nil {
			return nil, err
		}
		databases = append(databases, database)
	}
	return databases, nil
}

func (c *client) ShowTables(from ...string) (tables []string, _ error) {
	query := "SHOW TABLES"
	if len(from) != 0 {
		query = "SHOW TABLES FROM " + from[0]
	}
	rows, err := c.conn.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var table string
		if err := rows.Scan(&table); err != nil {
			return nil, err
		}
		tables = append(tables, table)
	}
	return tables, nil
}

func (c *client) DatabaseExists(database string) (bool, error) {
	return c.exists("SELECT COUNT() FROM system.databases WHERE name = ?", database)
}

func (c *client) TableExists(database, table string) (bool, error) {
	return c.exists("SELECT COUNT() FROM system.tables WHERE database = ? AND name = ?", database, table)
}

func (c *client) DictionaryExists(dictionary string) (bool, error) {
	return c.exists("SELECT COUNT() FROM system.dictionaries WHERE name = ?", dictionary)
}

func (c *client) exists(query string, args ...interface{}) (bool, error) {
	var count int
	if err := c.conn.QueryRow(query, args...).Scan(&count); err != nil {
		return false, err
	}
	return count == 1, nil
}

func (c *client) ReloadDictionary(dictionary string) error {
	if _, err := c.conn.Exec("SYSTEM RELOAD DICTIONARY " + quote(dictionary)); err != nil {
		return err
	}
	return nil
}

var _ ClickHouse = (*client)(nil)

func quote(v string) string {
	return "'" + strings.NewReplacer(`\`, `\\`, `'`, `\'`).Replace(v) + "'"
}
