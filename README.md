# ClickHouse testing suite

This is a small framework to help test Go and ClickHouse applications.

```go
clickhouse, err := check.Connect("native://127.0.0.1:9000?debug=0")
if err != nil {
    log.Fatal(err)
}
version, _ := clickhouse.Version();
if version.Less(&check.Version{19, 0, 0}) {
    log.Fatalf("unsupported version: %s", version)
}
```